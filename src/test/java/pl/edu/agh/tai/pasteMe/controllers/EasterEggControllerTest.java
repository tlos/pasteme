package pl.edu.agh.tai.pasteMe.controllers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EasterEggControllerTest {

	private static final String VIEW_NAME = "egg";
	
	private EasterEggController controller;

	@Before
	public void setUp() throws Exception {
		this.controller = new EasterEggController();

	}

	@Test
	public void testDoGet() {
		Assert.assertNotNull(controller);
		Assert.assertEquals(VIEW_NAME, controller.doGet());
		
	}

}
