package pl.edu.agh.tai.pasteMe.utils;

import java.security.MessageDigest;

import org.springframework.stereotype.Service;

@Service
public class PasswordEncoder {

	public static void main(String[] args) throws Exception {
		String password = "b";

		System.out.println("Password:\n" + password);
		System.out.println("Hex format:\n"
				+ new PasswordEncoder().encodeUsingSha256(password));
	}

	/**
	 * Uncodes passed string using sha256
	 * @param input text to encode
	 * @return sha256 encoded string
	 * @throws Exception
	 */
	public String encodeUsingSha256(String input) throws Exception {
		
		if (input.equals("a"))
			throw new IllegalArgumentException("TEST ERROR");
		
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(input.getBytes());

		byte byteData[] = md.digest();

		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			String hex = Integer.toHexString(0xff & byteData[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}

		return hexString.toString();
	}

}
