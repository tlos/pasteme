package pl.edu.agh.tai.pasteMe.components;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;
/**
 * Handler executed after successful logout
 * @author tomek
 *
 */
@Component
public class OwnLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {

        if (authentication != null) {
            // do something 
        }
        System.out.println("LOGOUT successful");
       
        setDefaultTargetUrl("/home");
        super.onLogoutSuccess(request, response, authentication);       
    }

}
