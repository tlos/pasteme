package pl.edu.agh.tai.pasteMe.db;

import java.util.List;
/**
 * DAO to user beans
 * @author tomek
 *
 */
public interface UserDAO {

	void insert(User user);

	List<User> getAllUsers();
}
