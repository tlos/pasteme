package pl.edu.agh.tai.pasteMe.storage.model;

/**
 * Bean needed to show errors in Views
 *
 */
public class ErrorBean {

	private Integer code;
	private String title;
	private String message;

	public ErrorBean() {
	}

	public ErrorBean(Integer code, String title, String message) {
		this.code = code;
		this.title = title;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
