package pl.edu.agh.tai.pasteMe.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pl.edu.agh.tai.pasteMe.utils.LogUtils;

@Controller
@RequestMapping("/home")
public class HomeController {
	private static final Logger log = Logger.getLogger(HomeController.class);

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView doGet(
			@ModelAttribute("login_error") String login_error,
			HttpServletRequest request) {
		LogUtils.reportMethodStart(log);
		ModelAndView mv = new ModelAndView("home");
		if (login_error != null && "t".equals(login_error)) {
			mv.addObject("login_error_msg", "Error when login, bad credentials?");
		}
		
		return mv;
	}
}
