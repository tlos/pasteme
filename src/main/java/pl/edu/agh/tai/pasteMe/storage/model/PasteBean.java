package pl.edu.agh.tai.pasteMe.storage.model;

import java.io.Serializable;

/**
 * Model for pastes
 *
 */
public class PasteBean implements Serializable{

	private static final long serialVersionUID = -6272701747994485324L;
	private String name;
	private String text;

	public static String getPath(String pasteName){
		return pasteName == null ? null : "/" + pasteName;
	}

	public PasteBean() {
	}

	public PasteBean(String name, String text) {
		setName(name);
		setText(text);
	}

	public PasteBean(String name) {
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		//this.name = new String(Charset.forName("UTF-8").encode(name).array());
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		//this.text = new String(Charset.forName("UTF-8").encode(text).array());;
		this.text = text;
	}

	public String getPath() {
		return getPath(name);
	}
	@Override
	public String toString() {
		return "{ name='" +name +"'\ntext='"+text+"'}";
	}
}
