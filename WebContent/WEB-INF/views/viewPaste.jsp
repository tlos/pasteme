<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">


<head>


<script src="<c:url value="/resources/js/jquery/jquery.js" />"
	type="text/javascript" charset="utf-8"></script>


<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" />

<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />" />
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"
	type="text/javascript" charset="utf-8"></script>

<jsp:include page="/WEB-INF/fragments/head.jsp" />
<title>List Pastes</title>
<title>List Pastes</title>

<style type="text/css" media="screen">
#editor {
	height: 500px;
	width: 80%;
	margin: auto;

}
#pastename{
	font-size: "150%";
	margin:auto;
	width: 80%;
}
</style>
<script src="<c:url value="/resources/js/ace/src-noconflict/ace.js" />"
	type="text/javascript" charset="utf-8"></script>


</head>

<body>
	<div class="container">
	<jsp:include page="/WEB-INF/fragments/header.jsp" />


	<div id="pastename">${pasteBean.name}</div>

	<div id="viewpaste">

		<!--<form:form method="GET" modelAttribute="pasteBean">
			Name:
			<form:input path="name" readonly="true" />
			<br><br> <form:textarea path="text" rows="30" cols="100"
						readonly="true" />
		</form:form>
	-->


		<div id="editor">${fn:escapeXml(pasteBean.text)}</div>


		<script>
			var editor = ace.edit("editor");
			editor.setTheme("ace/theme/chrome");
			editor.getSession().setMode("ace/mode/c_cpp");
			editor.setReadOnly(true);
			editor.setHighlightActiveLine(false);
		</script>

	</div>
	<jsp:include page="/WEB-INF/fragments/footer.jsp" />
	</div>
</body>

</html>