package pl.edu.agh.tai.pasteMe.controllers;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import pl.edu.agh.tai.pasteMe.storage.Storage;
import pl.edu.agh.tai.pasteMe.storage.StorageException;
import pl.edu.agh.tai.pasteMe.storage.model.PasteBean;

public class ViewPasteControllerTest {
	private static final String PASTE_NAME = "ble";
	private static final String PASTE_TEXT = "text";
	private static final String VIEW_NAME = "viewPaste";

	@Mock
	private Storage storage;

	@Autowired
	private WebApplicationContext wac;

    private ViewPasteController controller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.controller = new ViewPasteController(storage);

		Mockito.when(storage.download(null)).thenThrow(
				new StorageException());
		Mockito.when(storage.download(PASTE_NAME)).thenReturn(
				new PasteBean(PASTE_NAME, PASTE_TEXT));
	}
	
	@Test
	public void testinstance(){
		Assert.assertNotNull(controller);
	}
	


	@Test(expected=StorageException.class)
	public void testDoGetEmptyParam() throws Exception {
		String pasteName = "";
		controller.doGet(pasteName);
	}
	
	@Test(expected=StorageException.class)
	public void testDoGetNullParam() throws Exception {
		String pasteName = null;
		controller.doGet(pasteName);
	}
	
	@Test
	public void testDoGet() throws Exception {
		ModelAndView mv = controller.doGet(PASTE_NAME);
		Assert.assertEquals(VIEW_NAME, mv.getViewName());
		PasteBean p = (PasteBean) mv.getModel().get("pasteBean");
		Assert.assertNotNull(p);
		Assert.assertEquals(PASTE_NAME, p.getName());
		Assert.assertEquals(PASTE_TEXT, p.getText());
	}

}
