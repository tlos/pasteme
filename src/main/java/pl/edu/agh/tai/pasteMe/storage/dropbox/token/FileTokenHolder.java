package pl.edu.agh.tai.pasteMe.storage.dropbox.token;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FileTokenHolder implements TokenHolder {
	public static final String DEFAULT_TOKEN_FILE_PATH = "token.dat";
	private String token;
	private String tokenFilePath = DEFAULT_TOKEN_FILE_PATH;
	
	
	/**
	 * Get token, if token was not loaded it loads it from file
	 */
	@Override
	public String getToken() {
		if (token == null) {
			readTokenFromFile();
		}
		return token;
	}

	/**
	 * Puts new token to file
	 */
	@Override
	public void put(String token) {
		this.token = token;
		writeTokenToFile();
	}

	/**
	 * Sets file path for token. 
	 * If no pressed it is used DEFAULT_TOKEN_FILE_PATH
	 * @param tokenFilePath
	 */
	public void setTokenFilePath(String tokenFilePath) {
		this.tokenFilePath = tokenFilePath;
	}
	
	
	
	private void writeTokenToFile() {
		BufferedWriter writer = null;
		try {
			File file = new File(
					(getClass().getClassLoader().getResource(tokenFilePath).getFile()));

			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file)));
			writer.write(token);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	
	
	private void readTokenFromFile() {
		BufferedReader reader = null;
		try {
			File file = new File(
					(getClass().getClassLoader().getResource(tokenFilePath).getFile()));

			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(file)));
			this.token = reader.readLine();
		} catch (Exception e) {
			this.token = null;
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
