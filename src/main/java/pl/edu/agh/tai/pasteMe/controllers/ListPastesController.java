package pl.edu.agh.tai.pasteMe.controllers;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pl.edu.agh.tai.pasteMe.storage.Storage;
import pl.edu.agh.tai.pasteMe.storage.StorageException;
import pl.edu.agh.tai.pasteMe.utils.LogUtils;

@Controller
@RequestMapping("/listPastes")
public class ListPastesController {
	private static final Logger log = Logger
			.getLogger(ListPastesController.class);

	private Storage storage;

	@Autowired
	public ListPastesController(Storage storage) {
		this.storage = storage;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView doGet() throws StorageException {
		LogUtils.reportMethodStart(log);
		ModelAndView mv = new ModelAndView("listPastes");

		Collection<String> pastes = storage.list();
		mv.addObject("pastes", pastes);

		return mv;
	}
}
