package pl.edu.agh.tai.pasteMe.storage.dropbox.dao;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import pl.edu.agh.tai.pasteMe.storage.StorageException;
import pl.edu.agh.tai.pasteMe.utils.LogUtils;

import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxWriteMode;

public class SimpleDropboxDao implements DropboxDao {
	private static final Logger log = Logger.getLogger(SimpleDropboxDao.class);
	private DbxClient dropboxClient;

	public SimpleDropboxDao(DbxClient dropboxClient) {
		this.dropboxClient = dropboxClient;
	}

	@Override
	public void upload(String text, String pathInDropbox, boolean force)
			throws StorageException {
		LogUtils.reportMethodStart(log);
		if( pathInDropbox == null || pathInDropbox.isEmpty() || pathInDropbox.endsWith("/")){
			throw new StorageException("Wrong path!");
		}
		ByteArrayInputStream bin = new ByteArrayInputStream(text.getBytes());
		try {
			upload(pathInDropbox, bin, text.length(), force);
		} finally {
			try {
				bin.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String download(String pathInDropbox) throws StorageException {
		LogUtils.reportMethodStart(log);
		if( pathInDropbox == null || pathInDropbox.isEmpty() || pathInDropbox.endsWith("/")){
			throw new StorageException("Wrong path!");
		}
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		try {
			download(pathInDropbox, bout);
			return bout.toString();
		} finally {
			try {
				bout.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public Collection<String> listFiles(String path) throws StorageException {
		LogUtils.reportMethodStart(log);
		if( path == null || path.isEmpty()){
			throw new StorageException("Wrong path!");
		}
		log.debug("listFiles path=" + path);
		List<String> dirContent = new ArrayList<String>();
		try {
			DbxEntry.WithChildren listing = dropboxClient
					.getMetadataWithChildren(path);
			for (DbxEntry child : listing.children) {
				if( child.isFile()){
					dirContent.add(child.name);
				}
			}
		} catch (Exception e) {
			log.error("listing dirs from dropbox error path=" + path, e);
			throw new StorageException(e.getMessage(), e);
		}
		return dirContent;
	}
	
	

	@Override
	public boolean isFile(String path) throws StorageException {
		LogUtils.reportMethodStart(log);
		try {
			DbxEntry entry = dropboxClient.getMetadata(path);
			return (entry != null && entry.isFile());
		} catch (DbxException e) {
			log.error("checking if file exists, dropbox error path=" + path, e);
			throw new StorageException(e.getMessage(), e);
		}
	}

	private void upload(String dropboxPath, InputStream in, long bytes, boolean force)
			throws StorageException {
		try {
			log.debug("upload() dropboxPath=" + dropboxPath + " bytes=" + bytes + " force="+force);
			DbxEntry.File uploadedFile = dropboxClient.uploadFile(dropboxPath,
					force  ? DbxWriteMode.force() : DbxWriteMode.add(), bytes, in);
			log.info("uploaded successfully uploadedFile="
					+ uploadedFile.toString());
		} catch (Exception e) {
			log.error("uploading error", e);
			throw new StorageException(e.getMessage(), e);
		}
	}

	private void download(String dropboxPath, OutputStream out)
			throws StorageException {
		try {
			log.debug("get() dropboxPath=" + dropboxPath);
			DbxEntry.File downloadedFile = dropboxClient.getFile(dropboxPath,
					null, out);
			if (downloadedFile == null)
				throw new StorageException("File " + dropboxPath + " not found");
			log.info("got file successfully downloadedFile="
					+ downloadedFile.toString());
		} catch (Exception e) {
			log.error("getting from dropbox error", e);
			throw new StorageException(e.getMessage(), e);
		}
	}


	
	protected DbxClient getDropboxClient() {
		return dropboxClient;
	}
}
