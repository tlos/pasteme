package pl.edu.agh.tai.pasteMe.controllers;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import pl.edu.agh.tai.pasteMe.storage.StorageException;
import pl.edu.agh.tai.pasteMe.storage.model.ErrorBean;
import pl.edu.agh.tai.pasteMe.utils.LogUtils;

@ControllerAdvice
@EnableWebMvc
public class StorageExceptionController {
	private static final Logger log = Logger.getLogger(StorageExceptionController.class);
	
	
	
	@ExceptionHandler(StorageException.class)
	public ModelAndView handleStorageException(StorageException ex) {
		LogUtils.reportMethodStart(log);
		//log.error("Error", ex);
		ModelAndView mv = new ModelAndView("error-views/viewError");
		ErrorBean errorBean = new ErrorBean(StorageException.ERROR_CODE,
				"Error with storage", ex.getMessage());
		mv.addObject("error", errorBean);
		return mv;
	}
}
