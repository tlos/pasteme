<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<title>Page not found</title>

<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" />

<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />" />
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"
	type="text/javascript" charset="utf-8"></script>


</head>
<body>
<!-- 	<font size="+4">The required page was not found.</font> -->

<p align="center"><img src="<c:url value="/resources/img/page_not_found.jpg" />" style="margin:auto"></img></p>
</body>
</html>