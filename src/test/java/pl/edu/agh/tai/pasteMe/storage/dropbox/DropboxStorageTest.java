package pl.edu.agh.tai.pasteMe.storage.dropbox;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import pl.edu.agh.tai.pasteMe.storage.StorageException;
import pl.edu.agh.tai.pasteMe.storage.dropbox.dao.DropboxDao;
import pl.edu.agh.tai.pasteMe.storage.model.PasteBean;

public class DropboxStorageTest {

	private static DropboxStorage storage;
	private static final String PASTE_NAME = "paste";
	private static final String PASTE_TEXT = "text";

	private static DropboxDao dropboxDao;

	@SuppressWarnings("deprecation")
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		dropboxDao = Mockito.mock(DropboxDao.class);

		storage = DropboxStorage.getInstance();

		Mockito.when(dropboxDao.download(PasteBean.getPath(PASTE_NAME)))
				.thenReturn(PASTE_TEXT);
		Mockito.when(dropboxDao.download("")).thenReturn(PASTE_TEXT)
				.thenThrow(new StorageException());
		Mockito.when(dropboxDao.download(null)).thenThrow(
				new StorageException());
		Mockito.when(dropboxDao.download("/"))
				.thenThrow(new StorageException());

		Mockito.when(dropboxDao.listFiles("/")).thenReturn(
				Arrays.asList(new String[] { PASTE_NAME }));

		storage.setDropboxDao(dropboxDao);
	}

	@Test
	public void testGetInstance() {
		Assert.assertNotNull(storage);
	}

	@Test
	public void testUpload() throws StorageException {
		storage.upload(new PasteBean(PASTE_NAME, PASTE_TEXT));
		Mockito.verify(dropboxDao, Mockito.times(1)).upload(PASTE_TEXT,
				PasteBean.getPath(PASTE_NAME), true);
	}

	@Test
	public void testDownload() throws StorageException {
		PasteBean p = storage.download(PASTE_NAME);
		Assert.assertEquals(PASTE_NAME, p.getName());
		Assert.assertEquals(PASTE_TEXT, p.getText());
	}

	@Test
	public void testList() throws StorageException {
		Collection<String> r = storage.list();
		Assert.assertEquals(Arrays.asList(new String[] { PASTE_NAME }), r);
	}

}
