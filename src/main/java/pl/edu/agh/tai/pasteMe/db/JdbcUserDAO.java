package pl.edu.agh.tai.pasteMe.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class JdbcUserDAO extends JdbcDaoSupport implements UserDAO {

	private final String selectUsersSql = "SELECT USERNAME, ROLE_NAME FROM USERS JOIN ROLES ON USERS.ROLE_ID = ROLES.ROLE_ID;";

	private final String insertUserSql = "INSERT INTO users (username, password, enabled, role_id) VALUES (?, ?, 1, ?)";

	@Override
	public void insert(User user) {
		getJdbcTemplate().update(insertUserSql, new Object[] {user.getUsername(), user.getPassword(), user.getRole()});


	}

	@Override
	public List<User> getAllUsers() {
		List<User> users = new ArrayList<User>();

		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(
				selectUsersSql);
		for (Map<String, Object> row : rows) {
			User user = new User();
			user.setUsername((String) row.get("USERNAME"));
			user.setRole((String) row.get("ROLE_NAME"));

			users.add(user);
		}
		return users;
	}

}
