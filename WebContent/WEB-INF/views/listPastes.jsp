<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

<script src="<c:url value="/resources/js/jquery/jquery.js" />"
	type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" />

<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />" />
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"
	type="text/javascript" charset="utf-8"></script>


<style type="text/css" media="screen">
#listpastes {
    margin: auto;
    max-width: 60%;
    width: 60%;
}
</style>


<jsp:include page="/WEB-INF/fragments/head.jsp" />
<title>List Pastes</title>
</head>

<body>
	<div class="container">
	<jsp:include page="/WEB-INF/fragments/header.jsp" />

	
	<div id="listpastes">


		<table class="table table-striped table-bordered table-hover table-condensed">	
			<th>Pastes:</th>
			<c:forEach items="${pastes}" var="paste">
				<tr>
					<td><a href="viewPaste?pasteName=${paste}">${paste}</a></td>
				</tr>
			</c:forEach>
		</table>

	</div>
	
	<jsp:include page="/WEB-INF/fragments/footer.jsp" />
	
	</div>
</body>

</html>