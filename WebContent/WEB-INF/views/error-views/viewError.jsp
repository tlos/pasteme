<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>


	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>"${error.title}"</title>
</head>

<body>
<div id="error">

<p align="center"><img src="<c:url value="/resources/img/error.jpg" />" style="margin:auto"></img>
<h1>${error.code}: ${error.title}</h1>

${error.message}
</p>
</div>
</body>

</html>