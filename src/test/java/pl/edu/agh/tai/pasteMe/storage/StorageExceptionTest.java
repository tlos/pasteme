package pl.edu.agh.tai.pasteMe.storage;

import org.junit.Assert;
import org.junit.Test;


public class StorageExceptionTest {
	
	
	private final static String EX_MESSAGE = "msg";
	private final static Throwable EX_CAUSE = new Exception();
	private StorageException exception;
	
	@Test
	public void testStorageException() {
		exception = new StorageException();
		Assert.assertNotNull(exception);
	}

	@Test
	public void testStorageExceptionString() {
		exception = new StorageException("");
		Assert.assertNotNull(exception);
		Assert.assertEquals("", exception.getMessage());
		
		
		exception = new StorageException(EX_MESSAGE);
		Assert.assertNotNull(exception);
		Assert.assertEquals(EX_MESSAGE, exception.getMessage());
	}

	@Test
	public void testStorageExceptionThrowable() {
		exception = new StorageException(EX_CAUSE);
		Assert.assertNotNull(exception);
		Assert.assertEquals(EX_CAUSE, exception.getCause());
	
	}

	@Test
	public void testStorageExceptionStringThrowable() {
		exception = new StorageException(EX_MESSAGE,EX_CAUSE);
		Assert.assertNotNull(exception);
		Assert.assertEquals(EX_MESSAGE, exception.getMessage());
		Assert.assertEquals(EX_CAUSE, exception.getCause());
	}

}
