package pl.edu.agh.tai.pasteMe.controllers;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pl.edu.agh.tai.pasteMe.db.User;
import pl.edu.agh.tai.pasteMe.db.UserDAO;
import pl.edu.agh.tai.pasteMe.utils.LogUtils;
import pl.edu.agh.tai.pasteMe.utils.PasswordEncoder;

@Controller
@RequestMapping("/admin/addUser")
public class AddUserController {
	private static final String SUCCESS = "success";
	private static final String ADD_USER = "addUser";

	private static Logger log = Logger.getLogger(AddUserController.class);

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserDAO userDao;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView doGet(@ModelAttribute(SUCCESS) String status) {
		LogUtils.reportMethodStart(log);
		ModelAndView mv = new ModelAndView(ADD_USER, "user", new User());

		if (status != null) {
			if ("t".equals(status)) {
				mv.addObject("succes_msg", "Successfully added user!");
			} else if ("f".equals(status)) {
				mv.addObject("succes_msg", "Error when adding user...");
			}
		}

		List<User> users = userDao.getAllUsers();
		for (User u : users) {
			log.info(u);
		}
		mv.addObject("users", users);

		return mv;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView doPost(@ModelAttribute("user") User user) {
		LogUtils.reportMethodStart(log);
		ModelAndView mv = new ModelAndView("redirect:" + ADD_USER);
		String password = user.getPassword();
		if (!validateInput(user)) {
			mv.addObject(SUCCESS, "f");
			return mv;
		}

		try {
			user.setPassword(passwordEncoder.encodeUsingSha256(password));
			userDao.insert(user);
		} catch (Exception e) {
			log.error(e.getMessage());
			mv.addObject(SUCCESS, "f");
			return mv;
		}

		mv.addObject(SUCCESS, "t");
		return mv;
	}

	private boolean validateInput(User user) {
		if (user.getPassword().trim().isEmpty())
			return false;

		if (user.getUsername().trim().isEmpty()) {
			return false;
		}

		return true;
	}

}
