package pl.edu.agh.tai.pasteMe.storage.dropbox.dao;

import java.util.Collection;

import pl.edu.agh.tai.pasteMe.storage.StorageException;

public interface DropboxDao {

	/**
	 * Uploads text to dropbox file "pathInDropbox"
	 * 
	 * @param text
	 *            text to upload as file
	 * @param pathInDropbox
	 *            path where to upload file
	 * @param force
	 *            if true replaces existing file in dropbox <br>
	 *            if false and in dropbox exists file with such name <br>
	 *            adds sufix with number of instances of such filename in
	 *            dropbox
	 * @throws StorageException
	 */
	public void upload(String text, String pathInDropbox, boolean force)
			throws StorageException;

	
	/**
	 * Downloads file from pathInDropbox as String
	 * @param pathInDropbox  path to file to download 
	 * @return downloaded file as String
	 * @throws StorageException
	 */
	public String download(String pathInDropbox) throws StorageException;

	/**
	 * Lists files in dir - path
	 * @param path path to dir to be listed
	 * @return Collection of file's names in path
	 * @throws StorageException
	 */
	public Collection<String> listFiles(String path) throws StorageException;

	
	/**
	 * @param path
	 * @return true if put path exists and is file <br> else false 
	 * @throws StorageException
	 */
	public boolean isFile(String path) throws StorageException;
}
