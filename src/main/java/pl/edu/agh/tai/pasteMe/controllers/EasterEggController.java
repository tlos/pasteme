package pl.edu.agh.tai.pasteMe.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.edu.agh.tai.pasteMe.utils.LogUtils;

@Controller
@RequestMapping("/egg")
public class EasterEggController {
	private static final Logger log = Logger.getLogger(EasterEggController.class);
	@RequestMapping(method=RequestMethod.GET)
	public String doGet(){
		LogUtils.reportMethodStart(log);
		return "egg";
	}
}
