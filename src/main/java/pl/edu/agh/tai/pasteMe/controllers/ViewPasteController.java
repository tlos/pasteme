package pl.edu.agh.tai.pasteMe.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.edu.agh.tai.pasteMe.storage.Storage;
import pl.edu.agh.tai.pasteMe.storage.StorageException;
import pl.edu.agh.tai.pasteMe.storage.model.PasteBean;
import pl.edu.agh.tai.pasteMe.utils.LogUtils;

@Controller
@RequestMapping("/viewPaste")
public class ViewPasteController {

	private static final Logger log = Logger
			.getLogger(ViewPasteController.class);

	private Storage storage;

	@Autowired
	public ViewPasteController(Storage storage) {
		this.storage = storage;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView doGet(@RequestParam("pasteName") String pasteName)
			throws ServletRequestBindingException, StorageException {
		LogUtils.reportMethodStart(log);
		//String pasteName = ServletRequestUtils.getRequiredStringParameter(
		//		request, "pasteName");
		if( pasteName == null || pasteName.isEmpty() ){
			throw new StorageException("pasteName not entered!");
		}
		PasteBean paste = storage.download(pasteName);
		ModelAndView mv = new ModelAndView("viewPaste");
		mv.addObject("pasteBean", paste);
		return mv;
	}

}
