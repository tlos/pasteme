package pl.edu.agh.tai.pasteMe.controllers;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.ModelAndView;

import pl.edu.agh.tai.pasteMe.storage.Storage;
import pl.edu.agh.tai.pasteMe.storage.StorageException;

public class ListPastesControllerTest {

	private static final String PASTE_NAME = "ble";
	private static final String VIEW_NAME = "listPastes";

	@Mock
	private Storage storage;

	private ListPastesController controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.controller = new ListPastesController(storage);

		Mockito.when(storage.list()).thenReturn(Arrays.asList(PASTE_NAME));

	}

	@Test
	public void testListPastesController() {
		Assert.assertNotNull(controller);
	}

	@Test
	public void testDoGet() throws StorageException {
		ModelAndView mv = controller.doGet();
		Assert.assertEquals(VIEW_NAME, mv.getViewName());
		@SuppressWarnings("unchecked")
		Collection<String> pastes = (Collection<String>) mv.getModel().get(
				"pastes");
		Assert.assertNotNull(pastes);
		Assert.assertEquals(Arrays.asList(PASTE_NAME), pastes);

	}

	@Test(expected = StorageException.class)
	public void testStorageEx() throws StorageException {
		Mockito.when(storage.list()).thenThrow(new StorageException());
		controller.doGet();
	}

}
