package pl.edu.agh.tai.pasteMe.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pl.edu.agh.tai.pasteMe.storage.Storage;
import pl.edu.agh.tai.pasteMe.storage.StorageException;
import pl.edu.agh.tai.pasteMe.storage.model.PasteBean;
import pl.edu.agh.tai.pasteMe.utils.LogUtils;

@Controller
@RequestMapping("/admin/addPaste")
public class AddPasteController {
	private static final Logger log = Logger
			.getLogger(AddPasteController.class);

	private Storage storage;

	@Autowired
	public AddPasteController(Storage storage) {
		this.storage = storage;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView doGet() {
		LogUtils.reportMethodStart(log);
		ModelAndView mv = new ModelAndView("addPaste");
		mv.addObject("pasteBean", new PasteBean());
		return mv;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String doPost(@ModelAttribute("pasteBean") PasteBean pasteBean)
			throws StorageException {
		LogUtils.reportMethodStart(log);
		checkPasteANdThrowExceptionIfNeeded(pasteBean);
		log.debug(pasteBean.toString());
		storage.upload(pasteBean);
		return "redirect:/listPastes";
	}

	private static void checkPasteANdThrowExceptionIfNeeded(PasteBean pasteBean)
			throws StorageException {
		String name = pasteBean.getName();
		String text = pasteBean.getText();
		if (name == null || name.isEmpty()) {
			throw new StorageException("Name of paste is empty!");
		}
		if (text == null) {
			throw new StorageException("Paste text is NULL");
		}
	}

}
