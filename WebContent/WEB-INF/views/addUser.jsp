<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">



<head>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" />

<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />" />

<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"
	type="text/javascript" charset="utf-8"></script>

<script src="<c:url value="/resources/js/jquery/jquery.js" />"
	type="text/javascript" charset="utf-8"></script>


<title>Add User</title>

<jsp:include page="/WEB-INF/fragments/head.jsp" />


</head>


<body>
	<div class="container">
	<jsp:include page="/WEB-INF/fragments/header.jsp" />

	<h1>add user</h1>

	<div>
		<h3>${succes_msg}</h3>
	</div>


	<div id="addUser">
		<h4>Form to adding users</h4>
		<div class="input-group navbar-form form-group">
			<form:form method="POST" class="well"
				commandName="admin/addUser" modelAttribute="user">
				<div class="control-group">
					<table>
						<tr>
							<td><form:label path="username">Name</form:label></td>
							<td><form:input class="form-control" path="username" /></td>
						</tr>
						<tr>
							<td><form:label path="password">Password</form:label></td>
							<td><form:input autocomplete="false" class="form-control"
									path="password" /></td>
						</tr>
						<tr>
							<td><form:label path="role">User Type:</form:label></td>
							<td><form:select path="role">
									<form:option value="R_ADM">Admin</form:option>
									<form:option value="R_USR">User</form:option>
								</form:select></td>
						</tr>
						<tr>
							<td><input type="submit" value="Submit" id="submitBtn"
								class="btn btn-success" /></td>
						</tr>
					</table>
				</div>
			</form:form>

		</div>
	</div>

	<div id='listUsers'>
		<h4>All registered users:</h4>
		<table class="table">
		<tr><th>Name:</th><th>Role:</th></tr>
			<c:forEach items="${users}" var="user">
				<tr>
					<td>${fn:escapeXml(user.username)}</td>
					<td>${user.role}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	


	<jsp:include page="/WEB-INF/fragments/footer.jsp" />
	</div>
</body>
</html>