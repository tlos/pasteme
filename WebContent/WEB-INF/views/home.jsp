<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">



<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<title>Homepage</title>

<script src="<c:url value="/resources/js/jquery/jquery.js" />"
	type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" />

<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />" />
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"
	type="text/javascript" charset="utf-8"></script>


<jsp:include page="/WEB-INF/fragments/head.jsp" />

<style type="text/css" media="screen">
.form-signin {
	margin: 0 auto;
	max-width: 330px;
	padding: 15px;
}
</style>

</head>

<body>
	<div class="container">
	<jsp:include page="/WEB-INF/fragments/header.jsp" />



	<div class="muted text-center">
		<h1>PasteMe - store source code in one place</h1>

		<p>PasteMe is a place, where you can store your source code. Of course it supports such things as syntax highlighting.</p>
		<p>Need to share your source code? Use pasteMe!</p>

	</div>

	<div class="container">
		<h3>${login_error_msg}</h3>
	</div>

	<security:authorize access="isAnonymous()">
		<div class="container">
			<form name='f'
				action='${pageContext.request.contextPath}/j_spring_security_check'
				method='post' class="form-signin">

				<h2 class="form-signin-heading">Log in to view pastes</h2>

				<input type='text' name='j_username' value='' class="form-control"
					autofocus="" required="" placeholder="Username" />
				<input type='password' name='j_password' class="form-control"
					placeholder="Password" />

				<input name="submit" type="submit"
					class="btn btn-lg btn-primary btn-block" value="Login" />

			</form>
		</div>
	</security:authorize>

	<security:authorize access="isAuthenticated()">
		<div>
			<h3>
				Welcome
				<security:authentication property="principal.username" />
				on 'pasteMe' homepage!
			</h3>

			<security:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')">
				<h4>
					You can list and see all pastes, just <a href="listPastes">click!</a>
				</h4>
			</security:authorize>
			<security:authorize access="hasRole('ROLE_ADMIN')">
				<h4>
					You can also add new pastes - <a href="admin/addPaste">Add
						Paste</a>
				</h4>
				<h4>
					As admin, you can add new users - <a href="admin/addUser">Add
						User</a>
				</h4>
			</security:authorize>
		</div>
	</security:authorize>

	<jsp:include page="/WEB-INF/fragments/footer.jsp" />
	</div>
</body>

</html>

