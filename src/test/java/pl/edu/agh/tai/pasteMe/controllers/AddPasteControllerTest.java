package pl.edu.agh.tai.pasteMe.controllers;

import static org.junit.Assert.*;

import java.awt.Stroke;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import pl.edu.agh.tai.pasteMe.storage.Storage;
import pl.edu.agh.tai.pasteMe.storage.StorageException;
import pl.edu.agh.tai.pasteMe.storage.model.PasteBean;

public class AddPasteControllerTest {
	private static final String VIEW_NAME_GET = "addPaste";
	private static final String VIEW_NAME_POST = "redirect:/listPastes";

	private static final String PASTE_NAME = "ble";
	private static final String PASTE_TEXT = "text";

	@Mock
	private Storage storage;

	private AddPasteController controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.controller = new AddPasteController(storage);

	}

	@Test
	public void testAddPasteController() {
		Assert.assertNotNull(controller);
	}

	@Test
	public void testDoGet() {
		Assert.assertEquals(VIEW_NAME_GET, controller.doGet().getViewName());
	}

	@Test
	public void testDoPost() throws StorageException {
		PasteBean p = new PasteBean(PASTE_NAME, PASTE_TEXT);
		controller.doPost(p);
		Mockito.verify(storage, Mockito.times(1)).upload(p);
	}

	@Test(expected = StorageException.class)
	public void testDoPostNull() throws StorageException {
		PasteBean p = new PasteBean(null, PASTE_TEXT);
		controller.doPost(p);
	}

	@Test(expected = StorageException.class)
	public void testDoPostEmpty() throws StorageException {
		PasteBean p = new PasteBean("", PASTE_TEXT);
		controller.doPost(p);
	}

	@Test(expected = StorageException.class)
	public void testDoPostTextNull() throws StorageException {
		PasteBean p = new PasteBean(PASTE_NAME, null);
		controller.doPost(p);
	}
}
