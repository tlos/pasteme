package pl.edu.agh.tai.pasteMe.storage;

import java.util.Collection;

import pl.edu.agh.tai.pasteMe.storage.model.PasteBean;

public interface Storage {
	
	/**
	 * Uploads paste to Storage. If such paste exists it overwrite it
	 * 
	 * @param pasteBean  paste to upload
	 * @throws StorageException 
	 */
	public void upload(PasteBean pasteBean)
			throws StorageException;


	/**
	 * Downloads paste from Storage  If there is no such paste the<br>
	 * StorageException is thrown
	 * 
	 * @param pasteName name of paste to download
	 * @return  downloaded pase from storage
	 * @throws StorageException
	 */
	public PasteBean download(String pasteName) throws StorageException;

	/**
	 * Obtains the collection of pasteBeans names<br>
	 * if path is not valid StorageException is thrown.
	 * @return
	 * @throws StorageException
	 */
	public Collection<String> list() throws StorageException;

	
	/**
	 * Checks if Paste with name "pasteName" exists in Storage
	 * @param pasteName  name of paste to check
	 * @return true if paste with such name exists else false
	 * @throws StorageException
	 */
	public boolean isPasteExists(String pasteName) throws StorageException;
	
	
}
