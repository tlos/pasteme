DROP TABLE IF EXISTS roles;
CREATE TABLE roles (role_id VARCHAR(5) PRIMARY KEY,
    role_name VARCHAR(15) UNIQUE NOT NULL);

INSERT INTO roles(role_id, role_name) VALUES('R_ADM', 'ROLE_ADMIN');
INSERT INTO roles(role_id, role_name) VALUES('R_USR', 'ROLE_USER');

DROP TABLE IF EXISTS users;
CREATE TABLE users (id_key serial PRIMARY KEY,
    username VARCHAR(45) UNIQUE NOT NULL,
    password VARCHAR(100) NOT NULL,
    enabled INT(1) NOT NULL,
    role_id VARCHAR(5) references roles(role_id));
