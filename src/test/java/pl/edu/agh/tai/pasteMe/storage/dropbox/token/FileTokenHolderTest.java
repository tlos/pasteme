package pl.edu.agh.tai.pasteMe.storage.dropbox.token;

import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FileTokenHolderTest {
	private static String FILE_PATH = "tmp_test.txt";
	private static String TEST_TOKEN = "aaaa22324";
	private static FileTokenHolder holder;

	@Before
	public void setUpBeforeClass() throws Exception {
		holder = new FileTokenHolder();
	}

	@Test
	public void testPutGetToken() {
		holder.setTokenFilePath(FILE_PATH);
		Assert.assertNull(holder.getToken());
		holder.put(TEST_TOKEN);
		Assert.assertNotNull(holder.getToken());
		Assert.assertEquals(TEST_TOKEN, holder.getToken());

	}


	@Test
	public void testGetException() {
		holder.setTokenFilePath("");
		Assert.assertNull(holder.getToken());
	}

	@AfterClass
	public static void cleanUpAfterClass() throws IOException {
		File f = new File(FileTokenHolder.class.getClassLoader()
				.getResource(FILE_PATH).getFile());
		f.delete();
		f.createNewFile();
	}

}
