<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Forbidden</title>
	<jsp:include page="/WEB-INF/fragments/head.jsp" />
	
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" />

<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />" />
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"
	type="text/javascript" charset="utf-8"></script>
	
	
<style type="text/css" media="screen">
img{
	margin:auto;
	size: 50%;
	
}

</style>
</head>

<body>
	<jsp:include page="/WEB-INF/fragments/header.jsp" />
	<div>
<!-- 		<h1>Forbidden - you don't have access to that page</h1> -->
		<p align="center"><img src="<c:url value="/resources/img/you_shall_not_pass.png" />"></img></p>
	</div>
	<jsp:include page="/WEB-INF/fragments/footer.jsp" />
</body>

</html>

