<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<div class="container">
	<div>
		<h2>pasteMe - where code meets</h2>
	</div>
	<nav class="navbar navbar-default" role="navigation">
		<div class="collapse navbar-collapse navbar-ex1-collapse navbar-inner">
			<div class="container">
			<ul class="nav navbar-nav">
				<li><a href="home">Home</a></li>
				<security:authorize	access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')">
					<li><a href="listPastes">ListPastes</a></li>
				</security:authorize>
				<security:authorize access="hasRole('ROLE_ADMIN')">
					<li><a href="admin/addPaste">Add Paste</a></li>
					<li><a href="admin/addUser">Add User</a></li>
				</security:authorize>
				<security:authorize access="isAuthenticated()">
					<li><a href="logout">Log out</a></li>
				</security:authorize>
			</ul>
			</div>
		</div>
	</nav>
</div>