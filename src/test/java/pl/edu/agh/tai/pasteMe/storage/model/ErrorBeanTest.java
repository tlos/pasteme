package pl.edu.agh.tai.pasteMe.storage.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ErrorBeanTest {
	private static final Integer ERROR_CODE = 401;
	private static final String ERROR_MESSAGE = "ERROR";
	private static final String ERROR_TITLE = "TITLE";
	
	
	private ErrorBean errorBean;
	
	@Before
	public void setUp() throws Exception {
		errorBean = new ErrorBean();
	}

	@Test
	public void testErrorBean() {
		Assert.assertNotNull(errorBean);
	}

	@Test
	public void testErrorBeanIntStringString() {
		errorBean = new ErrorBean(ERROR_CODE, ERROR_TITLE, ERROR_MESSAGE);
		

		Assert.assertNotNull(errorBean);
		Assert.assertEquals(ERROR_CODE, errorBean.getCode());
		Assert.assertEquals(ERROR_TITLE, errorBean.getTitle());
		Assert.assertEquals(ERROR_MESSAGE, errorBean.getMessage());
		
		
		errorBean = new ErrorBean(null, null, null);
		Assert.assertNotNull(errorBean);
		Assert.assertNull(errorBean.getCode());
		Assert.assertNull(errorBean.getTitle());
		Assert.assertNull(errorBean.getMessage());
	}

	@Test
	public void testSetGetCode() {
		Assert.assertNull(errorBean.getCode());
		errorBean.setCode(ERROR_CODE);
		Assert.assertEquals(ERROR_CODE, errorBean.getCode());
	}



	@Test
	public void testSetGetTitle() {

		Assert.assertNull(errorBean.getTitle());
		errorBean.setTitle(ERROR_TITLE);
		Assert.assertEquals(ERROR_TITLE, errorBean.getTitle());
	}

	@Test
	public void testSetGetMessage() {
		Assert.assertNull(errorBean.getCode());
		errorBean.setMessage(ERROR_MESSAGE);
		Assert.assertEquals(ERROR_MESSAGE, errorBean.getMessage());
	}

}
