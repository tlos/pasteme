package pl.edu.agh.tai.pasteMe.storage;

public class StorageException extends Exception {
	public static final Integer ERROR_CODE = 1007; 
	
	private static final long serialVersionUID = 1053502085327943353L;
	

	public StorageException() {
	}

	public StorageException(String message) {
		super(message);
	}

	public StorageException(Throwable cause) {
		super(cause);
	}

	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}


}
