package pl.edu.agh.tai.pasteMe.db;

/**
 * Bean representing user
 * @author tomek
 *
 */
public class User {

	private String username;
	private String password;
	private String role;

	public User() {
		username = "";
		password = "";
		role = "";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password
				+ ", role=" + role + "]";
	}

}
