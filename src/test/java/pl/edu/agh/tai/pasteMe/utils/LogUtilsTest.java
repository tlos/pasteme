package pl.edu.agh.tai.pasteMe.utils;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class LogUtilsTest {
	private static Logger log;
	
	@BeforeClass
	public static void setUp() throws Exception {
		log = Logger.getLogger(LogUtilsTest.class);
	}

	@Test
	public void testReportMethodStart() {
		Assert.assertNotNull(log);
		LogUtils.reportMethodStart(log);
		
	}
		
	@Test
	public void testConstruct(){
		LogUtils l = new LogUtils();
		Assert.assertNotNull(l);
	}
	

}
