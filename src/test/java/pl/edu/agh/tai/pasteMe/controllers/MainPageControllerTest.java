package pl.edu.agh.tai.pasteMe.controllers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MainPageControllerTest {

	private static final String VIEW_NAME = "redirect:home";
	
	private MainPageController controller;
	
	@Before
	public void setUp() throws Exception {
		this.controller = new MainPageController();
	}

	@Test
	public void testDoGet() {
		Assert.assertNotNull(controller);
		Assert.assertEquals(VIEW_NAME, controller.doGet());
	}

}
