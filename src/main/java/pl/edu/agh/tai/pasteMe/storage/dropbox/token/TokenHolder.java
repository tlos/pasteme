package pl.edu.agh.tai.pasteMe.storage.dropbox.token;

public interface TokenHolder {
	/**
	 * @throws May throw RuntimeException if there was error during obtaining token from data source
	 * @return token
	 */
	public String getToken();
	/**
	 * 
	 * @throws May throw RuntimeException if there was error during storing token in data source
	 * @param token
	 */
	public void put(String token);
}
