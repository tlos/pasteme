package pl.edu.agh.tai.pasteMe.storage.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PasteBeanTest {
	private final static String PASTE_NAME = "myPaste";
	private final static String PASTE_TEXT = "myText\n";
	private PasteBean paste;

	@Before
	public void setUp() throws Exception {
		paste = new PasteBean();
	}

	@Test
	public void testGetPathStatic() {
		Assert.assertNull(PasteBean.getPath(null));
		Assert.assertEquals("/" + PASTE_NAME, PasteBean.getPath(PASTE_NAME));
	}

	@Test
	public void testPasteBean() {
		Assert.assertNotNull(paste);
	}
	
	@Test
	public void testContructorWithNameParam(){
		paste = new PasteBean(PASTE_NAME);
		Assert.assertNotNull(paste);
		Assert.assertEquals(PASTE_NAME, paste.getName());
	}
	
	@Test
	public void testContructorWithNameParamTextParam(){
		paste = new PasteBean(PASTE_NAME,PASTE_TEXT);
		Assert.assertNotNull(paste);
		Assert.assertEquals(PASTE_NAME, paste.getName());
		Assert.assertEquals(PASTE_TEXT, paste.getText());
	}
	
	

	@Test
	public void testSetGetName() {
		Assert.assertNull(paste.getName());
		paste.setName(PASTE_NAME);
		Assert.assertEquals(PASTE_NAME, paste.getName());
		paste.setName("");
		Assert.assertEquals("", paste.getName());
	}

	

	@Test
	public void testSetGetText() {
		Assert.assertNull(paste.getText());
		paste.setText(PASTE_TEXT);
		Assert.assertEquals(PASTE_TEXT, paste.getText());
		paste.setText("");
		Assert.assertEquals("", paste.getText());
	}

	@Test
	public void testGetPath() {
		Assert.assertNull(paste.getPath());
		paste.setName(PASTE_NAME);
		Assert.assertEquals("/" + PASTE_NAME, paste.getPath());
	}

	@Test
	public void testToString() {
		String toStringValue = "{ name='" +PASTE_NAME +"'\ntext='"+PASTE_TEXT+"'}";
		paste.setName(PASTE_NAME);
		paste.setText(PASTE_TEXT);
		Assert.assertEquals(toStringValue, paste.toString());
	}

}
