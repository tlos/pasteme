package pl.edu.agh.tai.pasteMe.storage.dropbox;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Locale;

import org.apache.log4j.Logger;

import pl.edu.agh.tai.pasteMe.storage.Storage;
import pl.edu.agh.tai.pasteMe.storage.StorageException;
import pl.edu.agh.tai.pasteMe.storage.dropbox.configuration.DropboxConfiguration;
import pl.edu.agh.tai.pasteMe.storage.dropbox.dao.DropboxDao;
import pl.edu.agh.tai.pasteMe.storage.dropbox.dao.SimpleDropboxDao;
import pl.edu.agh.tai.pasteMe.storage.model.PasteBean;
import pl.edu.agh.tai.pasteMe.utils.LogUtils;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;

/**
 * Singleton
 * 
 */
public class DropboxStorage implements Storage {
	private static final Logger log = Logger.getLogger(DropboxStorage.class);
	
	private DropboxConfiguration configuration = DropboxConfiguration
			.getInstance();
	private DbxClient dropboxClient;
	private DropboxDao dropboxDao;

	private static class Holder {
		private static final DropboxStorage instance = new DropboxStorage();
	}

	/**
	 * Metod to get instance of DropboxStroge Singleton
	 * @return Instance of configured Dropbox storage
	 * @throws RuntimeException
	 *             when configuration go wrong
	 */
	public static DropboxStorage getInstance() {
		return Holder.instance;
	}

	/**
	 * Metod uploads pasteBean to dropbox storage
	 */
	@Override
	public void upload(PasteBean pasteBean) throws StorageException {
		upload(pasteBean.getText(), pasteBean.getPath(), true);
	}

	/**
	 * Method to download paste from dropbox storage
	 */
	@Override
	public PasteBean download(String pasteName) throws StorageException {
		LogUtils.reportMethodStart(log);
		String pastePath = PasteBean.getPath(pasteName);
		log.debug("pastePath=" + pastePath);
		String text = dropboxDao.download(pastePath);
		return new PasteBean(pasteName, text);
	}
	
	/**
	 * Lists pastes in dropbox
	 */
	@Override
	public Collection<String> list() throws StorageException {
		LogUtils.reportMethodStart(log);
		return dropboxDao.listFiles("/");
	}
	

	/**
	 * Examines if paste exists in dropbox
	 */
	@Override
	public boolean isPasteExists(String pasteName) throws StorageException {
		return dropboxDao.isFile(PasteBean.getPath(pasteName));
	}
	

	/**
	 * 
	 * @deprecated
	 * @param dropboxDao
	 */
	public void setDropboxDao(DropboxDao dropboxDao) {
		this.dropboxDao = dropboxDao;
	}
	
	
	
	private void upload(String textToUpload, String filePathInStorage,
			boolean force) throws StorageException {
		LogUtils.reportMethodStart(log);
		log.debug("upload() filePathInStorage=" + filePathInStorage + " force="
				+ force + " textToUpload=" + textToUpload);
		dropboxDao.upload(textToUpload, filePathInStorage, force);
	}

	private DropboxStorage() {
		initialize();
	}

	private void initialize() {
		LogUtils.reportMethodStart(log);

		try {
			DbxAppInfo appInfo = new DbxAppInfo(configuration.getAppKey(),
					configuration.getAppSecret());
			DbxRequestConfig config = new DbxRequestConfig(
					configuration.getClientIdentifier(), Locale.getDefault()
							.toString());
			DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config,
					appInfo);
			String accessToken = configuration.getAccessToken();

			if (accessToken == null) {
				log.debug("access token == null -> true");
				String authorizeUrl = webAuth.start();
				log.info("authorizeURL=" + authorizeUrl);
				System.out.println("authorizeURL=" + authorizeUrl
						+ "\nwrite recived code:\n");
				String code = new BufferedReader(new InputStreamReader(
						System.in)).readLine().trim();
				DbxAuthFinish authFinish = webAuth.finish(code);
				accessToken = authFinish.accessToken;
				configuration.setAccessToken(accessToken);
			}
			log.debug("Got access token");
			dropboxClient = new DbxClient(config, accessToken);
			this.dropboxDao = new SimpleDropboxDao(dropboxClient);
			log.info("DropboxStorage Initialized successfully");

		} catch (Exception e) {
			log.error("Error during DropboxStorage initialization", e);
			throw new RuntimeException(
					"Error during DropboxStorage initialization. "
							+ e.getMessage(), e);
		}
	
	}
}
