package pl.edu.agh.tai.pasteMe.storage.dropbox.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import pl.edu.agh.tai.pasteMe.storage.dropbox.DropboxStorage;
import pl.edu.agh.tai.pasteMe.storage.dropbox.token.FileTokenHolder;
import pl.edu.agh.tai.pasteMe.storage.dropbox.token.TokenHolder;
import pl.edu.agh.tai.pasteMe.utils.LogUtils;

/**
 * Singleton<br>
 * {@link DropboxStorage} configuration holder/loader class
 * 
 * 
 */
public class DropboxConfiguration {
	private static class Holder {
		private static final DropboxConfiguration instance = new DropboxConfiguration();
	}

	public static final String CONFIG_FILE_PATH = "dropbox.dat";

	private static final Logger log = Logger
			.getLogger(DropboxConfiguration.class);

	private TokenHolder tokenHolder;

	/*
	 * Properties
	 */
	// APP_KEY -> dropbox.config.appKey
	private String appKey;
	// APP_SECRET -> dropbox.config.appSecret
	private String appSecret;
	// -> dropbox.config.clientIdentifier
	private String clientIdentifier;

	/**
	 * 
	 * @return Configured instance of {@link DropboxConfiguration}
	 * @throws RuntimeException
	 *             when configuration go wrong
	 */
	public static DropboxConfiguration getInstance() {
		return Holder.instance;
	}

	public String getAppKey() {
		return appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public String getClientIdentifier() {
		return clientIdentifier;
	}

	public String getAccessToken() {
		return this.tokenHolder.getToken();
	}

	public void setAccessToken(String accessToken) {
		this.tokenHolder.put(accessToken);
	}

	private DropboxConfiguration() {
		this.tokenHolder = new FileTokenHolder();
		initialize();
	}

	private void initialize() {
		LogUtils.reportMethodStart(log);
		try {
			loadConfigurationFromPropertiesFile();
		} catch (Exception e) {
			log.error("Error during loading configuration from file", e);
			throw new RuntimeException(
					"Error during loading configuration from file. "
							+ e.getMessage(), e);
		}
		log.debug("DropboxConfiguration initialized successfully");
	}

	private void loadConfigurationFromPropertiesFile() throws Exception {
		LogUtils.reportMethodStart(log);
		Properties properties = new Properties();
		FileInputStream in = null;
		try {
			log.debug("getResource = " + getClass().getClassLoader().getResource(CONFIG_FILE_PATH));
			File file = new File(
					(getClass().getClassLoader().getResource(CONFIG_FILE_PATH).getFile()));

			in = new FileInputStream(file);
			properties.load(in);
			this.appKey = properties.getProperty("dropbox.config.appKey");
			if (this.appKey == null)
				throw new Exception("Property dropbox.config.appKey not found");
			this.appSecret = properties.getProperty("dropbox.config.appSecret");
			if (this.appSecret == null)
				throw new Exception(
						"Property dropbox.config.appSecret not found");
			this.clientIdentifier = properties
					.getProperty("dropbox.config.clientIdentifier");
			if (this.clientIdentifier == null)
				throw new Exception(
						"Property dropbox.config.clientIdentifier not found");

		} finally {
			if (in != null)
				in.close();
		}
	}

}
