package pl.edu.agh.tai.pasteMe.controllers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import pl.edu.agh.tai.pasteMe.storage.StorageException;
import pl.edu.agh.tai.pasteMe.storage.model.ErrorBean;

public class StorageExceptionControllerTest {
	private static final String VIEW_NAME = "error-views/viewError";
	private static final String EX_MSG = "fail";

	private StorageException exception;
	private StorageExceptionController controller;
	@Before
	public void setUp() throws Exception {
		this.exception = new StorageException(EX_MSG);
		this.controller = new StorageExceptionController();
	}

	@Test
	public void testHandleStorageException() {
		Assert.assertNotNull(controller);
		ModelAndView mv = controller.handleStorageException(exception);
		Assert.assertEquals(VIEW_NAME, mv.getViewName());
		ErrorBean eb = (ErrorBean) mv.getModel().get("error");
		Assert.assertNotNull(eb);
		Assert.assertEquals(StorageException.ERROR_CODE, eb.getCode());
		Assert.assertTrue( eb.getMessage().contains(EX_MSG) );
		
	}

}
