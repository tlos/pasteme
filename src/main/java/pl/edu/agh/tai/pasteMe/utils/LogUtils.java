package pl.edu.agh.tai.pasteMe.utils;

import org.apache.log4j.Logger;

public class LogUtils {

	/**
	 * Logs method where is invocated
	 * @param log
	 */
	public static void reportMethodStart(Logger log){
		String message = "-> " + Thread.currentThread().getStackTrace()[2].getMethodName() + "();";
		log.info(message);
	}
	
}
