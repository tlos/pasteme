<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">



<head>

<script src="<c:url value="/resources/js/jquery/jquery.js" />"
	type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" />

<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />" />

<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"
	type="text/javascript" charset="utf-8"></script>


<title>Add Paste</title>

<jsp:include page="/WEB-INF/fragments/head.jsp" />


<script src="<c:url value="/resources/js/ace/src-noconflict/ace.js" />"
	type="text/javascript" charset="utf-8"></script>


<style type="text/css" media="screen">
#editor {
	height: 500px;
	width: 100%;	
	margin: auto;
}
#addpaste{
	width: 80%;
	margin: auto;
}
.addpastebtnpnl{
	margin: auto;
	
}
#namefield{
	width: 180px;
}

</style>
</head>


<body>
	<div class="container">
	<jsp:include page="/WEB-INF/fragments/header.jsp" />
	
	<div id="addpaste">
		<div id="editor">${fn:escapeXml(pasteBean.text)}</div>
		<div class="addpastebtnpnl input-group navbar-form form-group">
			<form:form method="POST" modelAttribute="pasteBean" id="addpaste"
				class="form-horizontal">
				<div class="control-group">
					<table>
						<tr>

							<td><form:input path="name" id="namefield"
									class="form-control" placeholder="Paste name"/></td>

							<form:input type="hidden" path="text" rows="30" cols="100"
								id="textfield"/>


							<td><input type="submit" value="Add paste" id="submitBtn"
								class="btn btn-success" /></td>
						</tr>
					</table>

				</div>
			</form:form>
		</div>
	</div>
	</div>

	<script type="text/javascript">
		var editor = ace.edit("editor");
		editor.setTheme("ace/theme/chrome");
		editor.getSession().setMode("ace/mode/c_cpp");
	</script>

	<script type="text/javascript">
		$("#addpaste").submit(function(event) {
			var name = $("#namefield").val();
			var text = editor.getValue();
			if (name.length == 0) {
				alert("You have to put paste name!");
				event.preventDefault();
				return;
			}
			var ifadd = true;
			if (text.length == 0) {
				ifadd = confirm("Are you sure to add empty paste?");
			}

			if (ifadd) {
				$("#textfield").val(text);
			} else {
				event.preventDefault();
			}
		});
	</script>


</body>
</html>